*COMANDO PARA VALIDAR O CONDOMINIO NA API*
`php -q vihu.php INSTALL`

*COMANDO PARA TECLADO VALIDAR CODIGO DE ACESSO*
`php -q vihu.php [ACTION] [CODE] [KEYBOARD]`

    ACTION: ACCESS ou SYNC
    - ACCESS: valida código de acesso
    - SYNC: Sincroniza acesso do servidor com arquivo local

    CODE: código digitado pelo usuário com 6 digitos
    KEYBOARD: número do teclado que o usuário digitou

*COMANDO DE REGISTRO DO CRON PARA SINCRONIZAR ACESSOS*
`* * * * * root php -f '/home/vihu/php/vihu.php' 'SYNC'`
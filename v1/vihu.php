<?php
    //RETIRA WARNINGS E NOTICE
    error_reporting(E_ERROR);
    
    //CONSTANTES
    const DOMAIN = 'api.vihu.com.br';
    // const DOMAIN = 'apihomologacao.vihu.com.br';
    const URL = 'https://'.DOMAIN;
    const ACCESS = '/home/vihu/php/vihu-access.json';
    const LOG = '/home/vihu/php/vihu-log.json';
    const DEBUG = true;

    const DELIVERY = "python /var/www/vihu/py/motoboy.py";
    const ACESSO_VISITANTE = "python /var/www/vihu/py/pulsarele.py";
    const ACESSO_LIBERADO = 'python /var/www/vihu/py/portao_l.py';
    const ACESSO_INVALIDO = 'python /var/www/vihu/py/recusado.py';

    function debug($value) {
        if (!DEBUG) return;

        if (!file_exists(LOG)) {
            $handle = fopen(LOG,'w+');
            fwrite($handle, '');
            fclose($handle);
        }
        
        file_put_contents(LOG, date("c") . " - " . $value . "\n", FILE_APPEND);
    }    
       
    function isConnected() {
        debug('CONNECTION: INIT CHECK');

        $connected = @fsockopen(DOMAIN, 80); //website, port  (try 80 or 443)
        if ($connected){
            $is_conn = true; //action when connected
            fclose($connected);
        } else {
            $is_conn = false; //action in connection failure
        }

        debug('CONNECTION: ' . $is_conn);

        return $is_conn;
    }    
    
    function callAPI($method, $url, $data = false) {
        $curl = curl_init();
    
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
    
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        // curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // curl_setopt($curl, CURLOPT_USERPWD, "username:password");
    
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    
        $result = curl_exec($curl);
    
        curl_close($curl);
    
        return $result;
    }

    function syncDown() {
        $accesses = json_decode(callAPI('GET', URL . '/api/access/sync-down'));
        $localData = json_decode(file_get_contents(ACCESS));
        
        if (!$localData->locked) {
            $localData = $accesses;
            updateLocalData(json_encode($localData));
        }
    }

    function syncUp() {
        $accessesToSync = [];
        $localData = json_decode(file_get_contents(ACCESS));

        foreach ( $localData->data->visitantes as $visitante){
            if ($visitante->sync) {
                array_push($accessesToSync, $visitante);
            }
        }
        
        $result = callAPI('POST', URL . '/api/access/sync-up', json_encode(["accesses" => $accessesToSync]));
    }

    function updateLocalData($data) {
        $handle = fopen(ACCESS,'w+');
        fwrite($handle, $data);
        fclose($handle);
    }

    function validateAccessLocal($_code, $_keyboard) {
        $localData = json_decode(file_get_contents(ACCESS));

        if (!$localData->data || $_code == '123456') {
            return base64_encode(ACESSO_INVALIDO);
        }

        //VERIFICA SE O CODIGO EXISTE PARA ALGUM MORADOR
        foreach ($localData->data->moradores as $morador) {
            if (password_verify($_code, $morador->password)) {
                return base64_encode(ACESSO_LIBERADO);
            }
        }
    
        //VERIFICA SE O CODIGO EXISTE PARA ALGUM VISITANTE
        foreach ($localData->data->visitantes as $visitante) {
            if ($visitante->code == $_code && validateDeadline($visitante)) {
                
                if ($visitante->status != "ativo") {
                    return base64_encode(ACESSO_INVALIDO);
                }
                               
                $__keyboard = ($visitante->gates == 'null') ? NULL : $visitante->gates;                
                foreach ($__keyboard as $k) {
                    if ($k->gate == $_keyboard) {
                        return base64_encode(ACESSO_INVALIDO);
                    }
                }                
                
                if ($visitante->type != 'delivery') {
                    $newKeyboard = [
                        'gate' => $_keyboard,
                        'datetime' => date("Y-m-d H:i:s")
                    ];                    

                    if ($__keyboard) {
                        array_push($__keyboard, $newKeyboard);
                        $visitante->gates = $__keyboard;
                    } else {
                        $visitante->gates = [$newKeyboard];
                    }

                    $visitante->validations = $visitante->validations + 1;
                    $visitante->sync = true;                   
                    
                    if (($localData->data->condominio->number_of_gates == $visitante->validations) || ($_keyboard == $localData->data->condominio->number_of_gates) || ($visitante->exit)) {
                        $visitante->status = 2;
                    }

                    //ATUALIZA ARQUIVO
                    updateLocalData(json_encode($localData));
                    
                    return base64_encode(ACESSO_LIBERADO);
                } else {
                    return base64_encode(ACESSO_INVALIDO);
                }
            }
        }
        
        return base64_encode(ACESSO_INVALIDO);
    }

    function validateDeadline($access) {
		$before = new DateTime($access->date.' '. $access->time, new DateTimeZone('America/Sao_Paulo'));
        $before->modify('-30 minutes');

		$after = new DateTime($access->date.' '. $access->time, new DateTimeZone('America/Sao_Paulo'));
        $after->modify('+3 hours');
        
        $now = new DateTime();
        $now = $now->setTimezone(new DateTimeZone('America/Sao_Paulo'));
		if ($now >= $before && $now <= $after) {
			return true;
		}

		return false;
    }

    //INICIO
    $code = null;
    $keyboard = null;

    debug('-------------');
    debug('INIT: ' . $argv[1]);    

    if (!file_exists(ACCESS)) {
        updateLocalData(json_encode([]));
    }
    
    if ($argv[1] == 'ACCESS') {
        $code = ($argv[2]) ? $argv[2] : null;
        $keyboard = ($argv[3]) ? $argv[3] : null;

        debug('ACCESS: ' . $code . ' | ' . $keyboard);
    
        if (!isConnected()) {
            debug('ACCESS: Call API');
            $result = callAPI('GET', URL . '/api/access/'.$code.'/'.$keyboard);
        } else {
            debug('ACCESS: Offline');
            $result = validateAccessLocal($code, $keyboard);
        }

        debug('ACCESS (result): ' . $result);
        
        echo $result;        
    } else if ($argv[1] == 'SYNC') {
        
        if (isConnected()) {
            syncUp();
            syncDown();
        }
    }
?>